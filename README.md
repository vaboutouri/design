# Collaborateur 

Nous avons travaillé en collaboration avec Louis Leroy afin de surmonté les principales difficultés que nous avons rencontré.

# Design 

Ce TP à été réalisé sur une puce NRF52-DK 

# Objectifs

Implémenté les services du KeyFinder listés sur le tableau suivant : https://docs.google.com/spreadsheets/d/1fHrDfWxQfxB20iuFjW3tHeSJP2p7XG9IPGQKW9I_084/edit?usp=sharing


Reussir à afficher les services via l'application NRF-Connect sur android (à l'aide de nos smartphone) avec pour chaque caractéristique, un UUID unique, une ou plusieurs propriété (Notify,Read ou Write) et une valeur cohérente si la caractéristique est en mode read.


Allumer une LED sur une écriture caractéristique et de déclencher un bip buzzer.

# Objectifs réalisés

Nous avons réussi à implémenté certain services du KeyFinder (les trois premières du tableau précédent)


Nous avons réussi à afficher ces caractéristiques sur nos smartphone via l'application NRF-Connect sur android comme le montre ces deux screens : 

https://image.noelshack.com/fichiers/2023/46/7/1700405231-img-0720.jpg
https://image.noelshack.com/fichiers/2023/46/7/1700405231-img-0721.jpg
Explications : Par exemple, la caractéristique avec l’UUID 0x1401 est en mode Notify et Read et on peut lire sa valeur qui est 0xAA (comme ce qui est écris dans le main.c).

# Objectifs non réalisés

Le dernier objectif (allumer la LED et déclencher le BIP BUZZER) n'a cependant pas été atteint. Nous avons sous-estimé le temps nécessaire à la réalisation de cette feature.

# Utiliser le projet

Il est primordial de s'assurer de bien posséder la version 1.2.6 de NRF Connect toolchain manager et la version 2.4.2 de Connect SDK.

Ces applications vont ensuite fonctionner de paire avec VS-Code.

Une fois avoir vérifié que vous possédiez les bonnes versions, il faut connecter la carte NRF52-DK.

Procédez à l'ouverture de VS-Code en ouvrant un projet de type peripheral.

Téléchargez le main.c qui se situe sur ce git lab et remplacé le par le main.c de votre projet sur VS-Code.

Vous pouvez build le projet afin de vérifier qu'il n'y a pas d'erreur. (cette opération est optionnelle.)

Après avoir effectuez toutes les étapes dans l'ordre vous pouvez à présent flash la carte.

Le projet est alors lancé.

# Voir les caractéristiques

Déverouillez votre smartphone et installer NRF-Connect.

Lancez l'application et approchez votre smartphone de la carte mise en tension.

Trouvez la balise associé à votre carte.

Cliquez sur cette balise et les caractéristiques de celle-ci devraient être affiché (comme sur les screens précédent).

# Aide

Nous nous sommes inspiré des templates de NRF-Connect pour mener à bien le projet. 
