
# Design 

Ce TP à été réalisé à l'aide de l'application Kicad et du site internet Farnell pour choisir les bon composants

# Objectifs

L'objectif était de réaliser la carte PCB de notre keyfinder avec les composants déja existant comme la pile et le bouton et de rajouter un autre composant au choix.


# Objectifs réalisés

Nous avons réussi à implémenter la pile, le support ainsi que le bouton.
Les connections filaires ont aussi été réaliser afin de concevoir une pcb fonctionnelle.


# Objectifs non réalisés

Cependant nous n'avons pas réussi à ajouter le composant supplémentaire.
La place pose un souci évident, il aurait fallu modifier le CAO mécanique et cela demande énormément de temps et d'investissement.
L'autre problème était l'alimentation car l'accéléromètre doit fonctionner en continue donc possède une consomation élevé qui aurait beaucoup réduit la durée de vie de la Pile et donc du KeyFinder (0.25A).
Mais nous avons quand même déterminé lequel nosu allons choisir.
Nous allons prendre un accéléromètre pour indiquer si le KeyFinder a pris un choc supérieru à un certain g.
Nous avons la fiche technique du produit et le lien d'achat:

https://fr.farnell.com/comus-assemtech/asls2/commuteur-acceleration-2g/dp/4229058

# Utiliser le projet

Il faut extraire le fichier .zip et le l'importer dans le logiciel KiCad.


